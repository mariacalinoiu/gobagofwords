package main

import (
	"fmt"
)

type WordOccurrence struct {
	word string
	count int
}

type AVLTree struct {
	root *AVLNode
}

func (dictionary *AVLTree) addWordOccurrence(word string, count int) {
	dictionary.root = dictionary.root.addWordOccurrence(word, count)
}

func (dictionary *AVLTree) getWordOccurrence(key string) int {
	return dictionary.root.getWordOccurrence(key)
}

func (dictionary *AVLTree) check() {
	dictionary.root.check()
}

func (dictionary *AVLTree) treeToMap(words *map[string]int) {
	dictionary.root.treeToMap(words)
}

type AVLNode struct {
	wordOccurrence WordOccurrence
	left   *AVLNode
	right  *AVLNode
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func (node *AVLNode) getHeight() int {
	if node != nil {
		return 1 + max(node.left.getHeight(), node.right.getHeight())
	}
	return 0
}

func (node *AVLNode) getBalanceFactor() int {
	return node.left.getHeight() - node.right.getHeight()
}

func (node *AVLNode) rotateLeft() *AVLNode {
	newRoot := node.right
	node.right = newRoot.left
	newRoot.left = node

	return newRoot
}

func (node *AVLNode) rotateRight() *AVLNode {
	newRoot := node.left
	node.left = newRoot.right
	newRoot.right = node

	return newRoot
}

func (node *AVLNode) balanceTree() *AVLNode {
	if node == nil {
		return node
	}

	balanceFactor := node.getBalanceFactor()
	if balanceFactor == 2 {
		if node.left.right.getHeight() > node.left.left.getHeight() {
			node.left = node.left.rotateLeft()
		}
		return node.rotateRight()
	} else if balanceFactor == -2 {
		if node.right.left.getHeight() > node.right.right.getHeight() {
			node.right = node.right.rotateRight()
		}
		return node.rotateLeft()
	}
	return node
}

func (node *AVLNode) addWordOccurrence(word string, count int) *AVLNode {
	if count == 0 {
		count = 1
	}

	if node == nil {
		return &AVLNode{WordOccurrence{
			word:  word,
			count: count,
		}, nil, nil}
	}

	if word == node.wordOccurrence.word {
		node.wordOccurrence.count++
	} else if word > node.wordOccurrence.word {
		node.right = node.right.addWordOccurrence(word, count)
	} else {
		node.left = node.left.addWordOccurrence(word, count)
	}

	return node.balanceTree()
}

func (node *AVLNode) getWordOccurrence(key string) int {
	if node == nil {
		return 0
	}
	if key == node.wordOccurrence.word {
		return node.wordOccurrence.count
	} else if key > node.wordOccurrence.word {
		return node.right.getWordOccurrence(key)
	} else {
		return node.left.getWordOccurrence(key)
	}
}

func (node *AVLNode) check () {
	if node != nil {

		fmt.Println(node.wordOccurrence.word, " ", node.wordOccurrence.count)
		if node.left != nil {
			fmt.Println("\tleft: ", node.left.wordOccurrence.word, " ", node.left.wordOccurrence.count)
		}
		if node.right != nil {
			fmt.Println("\tright: ", node.right.wordOccurrence.word, " ", node.right.wordOccurrence.count)
		}

		node.left.check()
		node.right.check()
	}
}

func (node *AVLNode) treeToMap (words *map[string]int) {
	if node != nil {
		node.left.treeToMap(words)
		node.right.treeToMap(words)
		(*words)[node.wordOccurrence.word] = node.wordOccurrence.count
	}
}

/*
func main() {
	dictionary := new(AVLTree)
	dictionary.addWordOccurrence("a", 0)
	dictionary.addWordOccurrence("b", 0)
	dictionary.addWordOccurrence("a", 0)
	dictionary.addWordOccurrence("c", 0)
	dictionary.addWordOccurrence("a", 0)
	dictionary.addWordOccurrence("d", 0)
	dictionary.addWordOccurrence("e", 0)
	dictionary.addWordOccurrence("r", 0)
	dictionary.addWordOccurrence("g", 0)
	dictionary.addWordOccurrence("aa", 0)
	dictionary.addWordOccurrence("g", 0)
	dictionary.addWordOccurrence("h", 0)
	dictionary.addWordOccurrence("x", 0)
	dictionary.addWordOccurrence("z", 0)
	dictionary.addWordOccurrence("y", 0)
	dictionary.check()
}
*/
