package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

func getWords(w http.ResponseWriter, r *http.Request, dictionary *AVLTree, logger *log.Logger) {

	//check(dictionary)
	params, ok := r.URL.Query()["keywords"]
	status := http.StatusOK
	http.StatusText(status)

	if !ok || len(params[0]) < 1 {
		status = http.StatusNotFound
		logger.Printf("Status: %d %s", status, http.StatusText(status))
		http.Error(w, "No parameter 'keywords' found.", status)
		return
	} else {
		jsonString := dictionary.getKeysForKeywords(params[0])

		_, err := w.Write([]byte(jsonString))

		if err != nil {
			status = http.StatusNotFound
			logger.Printf("Status: %d %s", status, http.StatusText(status))
			http.Error(w, err.Error(), status)
			panic(err)
		}

		logger.Printf("Status: %d %s", status, http.StatusText(status))
	}
}

func getDictionary(w http.ResponseWriter, r *http.Request, dictionary *AVLTree, logger *log.Logger) {

	//check(dictionary)
	body, err1 := dictionary.fromDictToJSON()
	_, err2 := w.Write(body)
	status := http.StatusOK
	http.StatusText(status)

	if err1 != nil {
		status = http.StatusNotFound
		logger.Printf("Status: %d %s", status, http.StatusText(status))
		http.Error(w, err1.Error(), status)
		panic(err1)
	} else if err2 != nil {
		status = http.StatusNotFound
		logger.Printf("Status: %d %s", status, http.StatusText(status))
		http.Error(w, err2.Error(), status)
		panic(err2)
	}

	logger.Printf("Status: %d %s", status, http.StatusText(status))
}

func postWords(w http.ResponseWriter, r *http.Request, dictionary *AVLTree, logger *log.Logger) {

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	status := http.StatusCreated
	http.StatusText(status)

	if err != nil {
		status = http.StatusBadRequest
		logger.Printf("Status: %d %s", status, http.StatusText(status))
		http.Error(w, err.Error(), status)
		panic(err)
	}

	dictionary.addToDictionary(string(body))

	_, err = w.Write([]byte("Message added."))

	if err != nil {
		status = http.StatusTeapot
		logger.Printf("Status: %d %s", status, http.StatusText(status))
		http.Error(w, err.Error(), status)
		panic(err)
	}

	logger.Printf("Status: %d %s", status, http.StatusText(status))
}
