package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func setup(logger *log.Logger, dictionary *AVLTree) *http.Server {
	server := newServer(dictionary, logWith(logger))
	return &http.Server{
		Addr:         ":8080",
		Handler:      server,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
}

func newServer(dictionary *AVLTree, options ...Option) *Server {
	s := &Server{logger: log.New(ioutil.Discard, "", 0)}

	for _, o := range options {
		o(s)
	}

	s.mux = http.NewServeMux()

	s.mux.HandleFunc("/getWords",
		func(w http.ResponseWriter, r *http.Request) {
			getWords(w, r, dictionary, s.logger)
		})
	s.mux.HandleFunc("/getDictionary",
		func(w http.ResponseWriter, r *http.Request) {
			getDictionary(w, r, dictionary, s.logger)
		})
	s.mux.HandleFunc("/addText",
		func(w http.ResponseWriter, r *http.Request) {
			postWords(w, r, dictionary, s.logger)
		})

	return s
}

type Option func(*Server)

func logWith(logger *log.Logger) Option {
	return func(s *Server) {
		s.logger = logger
	}
}

type Server struct {
	mux    *http.ServeMux
	logger *log.Logger
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.log("Method: %s, Path: %s", r.Method, r.URL.Path)
	s.mux.ServeHTTP(w, r)
}

func (s *Server) log(format string, v ...interface{}) {
	s.logger.Printf(format+"\n", v...)
}

func main() {

	logger := log.New(os.Stdout, "", 0)
	dictionary := new(AVLTree)
	hs := setup(logger, dictionary.readDictionary())
	logger.Printf("Listening on http://localhost%s\n", hs.Addr)

	go func() {
		if err := hs.ListenAndServe(); err != nil {
			logger.Println(err)
		}
	}()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	<-signals
	dictionary.writeDictionary()
	logger.Println("Wrote dictionary to file.")
	os.Exit(0)
}
