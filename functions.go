package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

func formatMessage(message string) string {
	message = strings.ToLower(message)

	reg, err := regexp.Compile("[^a-zA-Z0-9 ]+")
	if err != nil {
		panic(err)
	}
	message = reg.ReplaceAllString(message, "")

	return message
}

func (dictionary *AVLTree) addToDictionary(message string) *AVLTree {
	message = formatMessage(message)
	words := strings.Fields(message)

	for _, word := range words {
		dictionary.addWordOccurrence(word, 0)
		//check(dictionary)
	}

	return dictionary
}

func (dictionary *AVLTree) getKeysForKeywords(keywordString string) string {
	keywordString = formatMessage(keywordString)
	keywords := strings.Fields(keywordString)
	words := make(map[string]int)

	for _, word := range keywords {
		count := dictionary.getWordOccurrence(word)
		words[word] = count
	}

	jsonString, _ := json.Marshal(words)
	return string(jsonString)
}

func (dictionary *AVLTree) fromJSONToDict(data []byte) {
	words := make(map[string]int)
	err := json.Unmarshal(data, &words)

	if err == nil {
		for word, count := range words {
			dictionary.addWordOccurrence(word, count)
		}
	}
}

func (dictionary *AVLTree) fromDictToJSON() ([]byte, error) {
	words := make(map[string]int)
	dictionary.treeToMap(&words)
	jsonString, err := json.Marshal(words)
	if err != nil {
		return nil, err
	}
	return jsonString, nil
}

func (dictionary *AVLTree) readDictionary() *AVLTree {
	text, _ := ioutil.ReadFile("db.txt")
	dictionary.fromJSONToDict(text)
	return dictionary
}

func (dictionary *AVLTree) writeDictionary() {
	file, err := os.Create("db.txt")
	if err != nil {
		panic(err)
	} else {
		defer file.Close()
	}

	jsonString, err := dictionary.fromDictToJSON()
	if err != nil {
		panic(err)
	}

	_, err = file.Write(jsonString)
	if err != nil {
		panic(err)
	}
}
